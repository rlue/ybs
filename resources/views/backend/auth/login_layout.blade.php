<!DOCTYPE html>
<html class='no-js' lang=''>
<head>
  <meta charset='utf-8'>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta content='{{ csrf_token() }}' name='_token'>
  <title>@yield('title', 'Default Title')</title>
  <meta content='@yield('description', '')' name='description'>
  <meta content='@yield('author', '')' name='author'>
  @yield('meta')

  @yield('before-styles')

  {!! css([
    'bower_components/fontawesome/css/font-awesome.min',
    'bower_components/simple-line-icons/css/simple-line-icons',
    'css/style',
  ], 'assets/backend/') !!}

  @yield('styles')

</head>
<body class="app flex-row align-items-center">

  <div class="container">
      @yield('content')
  </div>

<!-- Bootstrap and necessary plugins -->
{!! js([
  "jquery/dist/jquery.min",
  "tether/dist/js/tether.min",
  "bootstrap/dist/js/bootstrap.min",
  "pace/pace.min",
], 'assets/backend/bower_components/') !!}
<!-- Plugins and scripts required by all views -->
{!! js([
  "chart.js/dist/Chart.min",
], 'assets/backend/bower_components/') !!}
<!-- GenesisUI main scripts -->
{!! js([
"js/coreui",
], 'assets/backend/') !!}

@yield('scripts')
</body>
</html>
