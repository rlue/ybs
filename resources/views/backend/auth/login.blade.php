@extends('backend.auth.login_layout')


@section('content')
  <div class="row justify-content-center">
      <div class="col-md-5">
          <div class="card-group mb-0">
              <div class="card p-4">
                  <div class="card-block">

                      @include('backend.partials.errors')

                      <h1>Login</h1>
                      <p class="text-muted">Sign In to your account</p>
                      <form action="{{ route('admin.showLogin') }}" method="POST" role="form">
                        {!! csrf_field() !!}
                        <div class="form-group {{ $errors->has('email') ? ' has-danger' : '' }}">
                          <div class="input-group mb-3">
                              <span class="input-group-addon"><i class="fa fa-envelope-open-o"></i>
                              </span>
                              <input type="email" name="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
                          </div>
                        </div>
                        <div class="form-group {{ $errors->has('email') ? ' has-danger' : '' }}">
                          <div class="input-group mb-4">
                              <span class="input-group-addon"><i class="icon-lock"></i>
                              </span>
                              <input type="password" name="password" class="form-control" placeholder="Password">
                          </div>
                        </div>

                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <button type="submit" class="btn btn-primary px-4">Login</button>
                            </div>
                            <div class="col-6 text-right">
                                {{-- <a href="" class="btn btn-link px-0">Forgot password?</a> --}}
                            </div>
                        </div>
                        </form>
                  </div>
              </div>
          </div>
      </div>
  </div>
@stop