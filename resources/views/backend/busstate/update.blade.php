@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-align-justify"></i> Update Gate
          </div>
          <div class="card-block">
         {!! Form::open(['route' => (['gatekeeper.update',$gatekeeper->gatekeeper_id]),'method' => 'PUT','class' => 'form-2orizontal','autocomplete' => 'false']) !!}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="gatekeeper_name" class="form-label">Gate Name </label>
                   <input type="text" name="gatekeeper_name" class="form-control"  required="required" placeholder="Enter Gate Keeper Name" value="{{$gatekeeper->gatekeeper_name}}">
                  </div>
                  <div class="form-group">
                   <label for="gatekeeper_password" class="form-label">Password</label>
                   <input type="password" name="gatekeeper_password" class="form-control" value="{{old('gatekeeper_password')}}"  placeholder="Enter Password">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="gatekeeper_password" class="form-label">Service No</label>
                   <select name="service_number" class="form-control">
                     <option value="selected">Choose Service No</option>
                     @foreach($bus as $b)
                      @if($gatekeeper->service_number == $b->id)
                        <option value="{{$b->id}}" selected="selected">{{$b->name}}</option>
                      @else
                        <option value="{{$b->id}}" >{{$b->name}}</option>
                      @endif
                     @endforeach
                   </select>
                  </div>
                  <div class="form-group">
                   <label for="gatekeeper_accountName" class="form-label">Account Name</label>
                   <input type="text" name="gatekeeper_accountName" class="form-control" required="required" placeholder="Enter Account Name" value="{{$gatekeeper->gatekeeper_accountName}}">
                  </div>
                </div>
              </div>
              <hr>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
            <a class="btn btn-default" href="{{ route('gatekeeper.index') }}" role="button">Cancel</a>
            {!! Form::close() !!}
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection



@push('scripts')
<script>

/* $('#bus-stops').select2({
    tags: true
 });

$("#bus-stops").on("select2:select", function (evt) {
  var element = evt.params.data.element;
  var $element = $(element);

  $element.detach();
  $(this).append($element);
  $(this).trigger("change");
});*/

</script>
@endpush
