@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-align-justify"></i> Create Gate
          </div>
          <div class="card-block">
            {!! Form::open(['route' => 'gatekeeper.store','method' => 'POST','class' => 'form-2orizontal']) !!}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="gatekeeper_name" class="form-label">Gate Name</label>
                   <input type="text" name="gatekeeper_name" class="form-control" value="{{old('gatekeeper_name')}}" required="required" placeholder="Enter Gate Keeper Name">
                  </div>
                  <div class="form-group">
                   <label for="gatekeeper_password" class="form-label">Password</label>
                   <input type="password" name="gatekeeper_password" class="form-control" value="{{old('gatekeeper_password')}}" required="required" placeholder="Enter Password">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="service_number" class="form-label">Service No</label>
                   <select name="service_number" class="form-control">
                     <option value="selected">Choose Service No</option>
                     @foreach($bus as $b)
                        <option value="{{$b->id}}">{{$b->name}}</option>
                     @endforeach
                   </select>
                  </div>
                  <div class="form-group">
                   <label for="gatekeeper_accountName" class="form-label">Account Name</label>
                   <input type="text" name="gatekeeper_accountName" class="form-control" value="{{old('gatekeeper_accountName')}}" required="required" placeholder="Enter Account Name">
                  </div>
                </div>
              </div>
              <hr>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a class="btn btn-default" href="{{ route('gate.index') }}" role="button">Cancel</a>
            {!! Form::close() !!}
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection


@push('scripts')
<script>
</script>
@endpush
