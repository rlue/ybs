@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-bus"></i> Bus State
              <div class="pull-right">
                  
              </div>
          </div>
          <div class="card-block">
              <table class="table table-bordered table-striped table-condensed datatables" id="township-table">
                  <thead>
                      <tr>
                          <th width="5%">No</th>
                          <th>Service No</th>
                          <th>Bus Number</th>
                          <th>Time</th>
                          <th>Date</th>
                          <th>In/Out</th>
                      </tr>
                  </thead>
                  <tbody>

                      @foreach ($state as $key => $gate)
                        <tr>
                          <td> {{ ++$key }}</td>
                          <td>{{ $gate->bus->name}}</td>
                           <td>{{ $gate->busState_busno}}</td>
                            <td>{{ $gate->busState_time}}</td>
                            <td>{{$gate->busState_date}}</td>
                            <td>@if($gate->busState_onoff == 1)
                              In
                            @else
                             Out
                            @endif
                            </td>
                            
                          
                        </tr>
                      @endforeach
                  </tbody>
              </table>
              <nav>
                 {{--  {!! $gates->links('vendor.pagination.bootstrap-4') !!} --}}
              </nav>
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection

