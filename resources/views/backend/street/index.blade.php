@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-bus"></i> Streets
              <div class="pull-right">
                  <a href="{{ route('street.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
              </div>
          </div>
          <div class="card-block">
              <table class="table table-bordered table-striped table-condensed datatables">
                  <thead>
                      <tr>
                          <th width="5%">No</th>
                          <th>Name</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach ($streets as $key => $street)
                        <tr>
                          <td> {{ ++$key }}</td>
                          <td>{{ $street->name}}</td>
                          <td>
                            <a href="{{ route('street.edit',['bus' => $street]) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => route('street.destroy',['street' => $street]),
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-sm',
                                        'title' => 'Delete',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                          </td>
                        </tr>
                      @endforeach
                  </tbody>
              </table>
              <nav>

              </nav>
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection
