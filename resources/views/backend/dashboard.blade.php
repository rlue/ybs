@extends('backend.layout')

@section('content')
  <div class="container-fluid">
      <div class="row">
        <div class="col-sm-6 col-lg-3">
          <div class="card card-inverse card-success">
            <div class="card-block pb-0">
              <h4 class="mb-0">
                {{ $data['totalBus'] }}
              </h4>              
              <p>
              Total Bus</p>
            </div>
          </div>
        </div>
        <div class="col-sm-6 col-lg-3">
          <div class="card card-inverse card-info">
            <div class="card-block pb-0">
              <h4 class="mb-0">
                {{ $data['totalBusStop'] }}
              </h4>              
              <p>
              Total Bus Stops</p>
            </div>
          </div>
        </div>
      </div>  
  </div>
@endsection
