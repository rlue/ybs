@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-align-justify"></i> Crete User
          </div>
          <div class="card-block">
            {!! Form::open(['route' => 'user.store','method' => 'POST','class' => 'form-2orizontal']) !!}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="name" class="form-label">Name</label>
                   <input type="text" name="name" class="form-control" value="" required="required">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="email" class="form-label">Email</label>
                   <input type="text" name="email" class="form-control" value="" required="required">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="password" class="form-label">Password</label>
                   <input type="password" name="password" id="password" class="form-control" value="" >
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="roles" class="form-label">Roles</label>
                    {!! Form::select('roles[]',$roles,'',['class' => 'form-control','id' => 'roles','multiple' => 'true']) !!}
                  </div>
                </div>
              </div>
              <hr>
              {{-- Don't delete  --}}
{{--               <div class="row">
                <div class="col-md-12 form-group">
                   @foreach (config('backend.permissions') as $key => $permission)
                        <h5>{{ ucwords($key) }} Access </h5>
                        <div class="row">
                          @foreach ($permission as $key)
                              <div class="col-sm-3">
                                <div class="checkbox-custom checkbox-success">
                                   <input class="permission-name" type="checkbox" id="{{ $key }}" value="{{ $key }}" name="permissions[]" >
                                  <label for="{{$key}}">{{ $key }}</label>
                                </div>
                              </div>
                          @endforeach
                        </div>
                   @endforeach
                </div>
              </div> --}}
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a class="btn btn-default" href="{{ route('user.index') }}" role="button">Cancel</a>
            {!! Form::close() !!}
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection


@push('scripts')
<script>
$('#roles').select2({
 placeholder: 'Select Roles'
});

</script>
@endpush
