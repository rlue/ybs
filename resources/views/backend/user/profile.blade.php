@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-align-justify"></i> Update Profile
          </div>
          <div class="card-block">
         {!! Form::open(['route' => (['user.updateProfile',$user]),'class' => 'form-2orizontal','autocomplete' => 'false']) !!}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="name" class="form-label">Name</label>
                   <input type="text" name="name" class="form-control" value="{{ $user->name }}" required="required">
                    {{ method_field('PUT') }}
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="email" class="form-label">Email</label>
                   <input type="text" name="email" class="form-control" value="{{ $user->email }}" required="required">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="password" class="form-label">Password</label>
                   <input type="password" name="password" id="password" class="form-control" autocomplete="false">
                  </div>
                </div>
              </div>
              <hr>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection

