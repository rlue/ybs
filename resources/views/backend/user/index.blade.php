@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-align-justify"></i> Users
              <div class="pull-right">
                  <a href="{{ route('user.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
              </div>
          </div>
          <div class="card-block">
              <table class="table table-bordered table-striped table-condensed">
                  <thead>
                      <tr>
                          <th>Username</th>
                          <th>Email</th>
                          <th>Role</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach ($users as $user)
                        <tr>
                          <td>{{ $user->name}}</td>
                          <td>{{ $user->email}}</td>
                          <td></td>
                          <td>
                            @can('update_user')
                            <a href="{{ route('user.edit',['user' => $user]) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                            @endcan
                            @can('delete_user')
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => route('user.destroy',['user' => $user]),
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-sm',
                                        'title' => 'Delete',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                            @endcan
                          </td>
                        </tr>
                      @endforeach
                  </tbody>
              </table>
              <nav>
                  {!! $users->links() !!}
              </nav>
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection
