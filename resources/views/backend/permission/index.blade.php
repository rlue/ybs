@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12">
      @can('create_permission')
        @if (count($new_permissions))
          <div class="card">
            <div class="card-header">
              New Permissions
            </div>
            <div class="card-block">
               <form action="" method="POST" role="form">
                  {!! csrf_field() !!}
                  <div class="row">
                      @foreach ($new_permissions as $permission)
                        <div class="col-md-3">
                          <div class="checkbox-custom checkbox-success">
                             <input class="permission-name" type="checkbox" id="{{ $permission }}" value="{{ $permission }}" name="permissions[]" >
                            <label for="{{$permission}}">{{ $permission }}</label>
                          </div>
                        </div>
                      @endforeach
                  </div>
                 <button type="submit" class="btn btn-primary">Save</button>
               </form>
            </div>
          </div>
        @endif
      @endcan
      <div class="card">
          <div class="card-header">
              <i class="fa fa-align-justify"></i> Permissions
          </div>
          <div class="card-block">
              <table class="table table-bordered table-striped table-condensed">
                  <thead>
                      <tr>
                          <th>Name</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach ($permissions as $permission)
                        <tr>
                          <td>{{ $permission->name}}</td>
                          <td>
                            @can('delete_permission')
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => route('permission.destroy',['permission' => $permission]),
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-sm',
                                        'title' => 'Delete',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                            @endcan
                          </td>
                        </tr>
                      @endforeach
                  </tbody>
              </table>
              <nav>
                  {!! $permissions->links() !!}
              </nav>
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection
