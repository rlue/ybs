@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12" id="app">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-align-justify"></i> Update Route
          </div>
          <div class="card-block">
            {!! Form::open(['route' => ['route.map'],'method' => 'POST','class' => 'form-horizontal','id' => 'route-form','target' => '_blank']) !!}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <input type="hidden" name="formSubmit" value="1">
                    <label for="bus" class="form-label">Bus</label>
                    <input type="text" name="bus" class="form-control" value="{{ $bus->name }}" required="required" readonly>
                    <input type="hidden" name="bus_id" value="{{ $bus->id }}">
                    <input type="hidden" name="route_start_to_end" id="route_start_to_end" value="">
                    <input type="hidden" name="route_end_to_start" id="route_end_to_start" value="">
                  </div>
                </div>
              </div>
{{--               <pre>
                @{{ list }}
              </pre> --}}
              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label for="bus_stops">Forwards Bus Stops</label>
                     <v-select :options="busstops" v-model="item" placeholder="Select Bus Stop"></v-select>
                  </div>
                </div>
                <div class="col-md-1 margin-top-30">
                   <button class="btn btn-primary btn-sm" v-on:click="addItem">
                    <i class="fa fa-plus" ></i>Add
                   </button>
                </div>
                <div class="col-md-5">
                  <div class="form-group">
                    <label for="bus_stops">Return Bus Stops</label>
                     <v-select :options="busstops" v-model="item1" placeholder="Select Bus Stop"></v-select>
                  </div>
                </div>
                <div class="col-md-1 margin-top-30">
                   <button class="btn btn-primary btn-sm" v-on:click="addItem1">
                    <i class="fa fa-plus"></i>Add
                   </button>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <draggable class="list-group" element="ul" v-model="list" :options="dragOptions" :move="onMove" @start="isDragging=true" @end="isDragging=false">
                    <transition-group type="transition" :name="'flip-list'">
                      <li class="list-group-item" v-for="(element, index) in list" :key="element.order">
                        <span class="text-primary">@{{ index + 1 }} . </span>
                        <i class="fa fa-map-marker"></i>
                        <input type="hidden" name="bus_ids[]" v-model="element.id"  >
                        <i :class="element.fixed? 'fa fa-anchor' : 'glyphicon glyphicon-pushpin'" @click=" element.fixed=! element.fixed" aria-hidden="true"></i>
                        @{{element.label}}
                        <a class="text-danger pull-right" v-on:click="removeItem(index)"><i class="fa fa-trash-o"></i></a>
                      </li>
                    </transition-group>
                  </draggable>
                </div>
                <div class="col-md-6">
                  <draggable class="list-group" element="ul" v-model="list1" :options="dragOptions" :move="onMove" @start="isDragging=true" @end="isDragging=false">
                    <transition-group type="transition" :name="'flip-list'">
                      <li class="list-group-item" v-for="(element, index) in list1" :key="element.order">
                        <span class="text-primary">@{{ index + 1 }} . </span>
                        <input type="hidden" name="bus_ids1[]" v-model="element.id"  >
                        <i class="fa fa-map-marker"></i>
                        <i :class="element.fixed? 'fa fa-anchor' : 'glyphicon glyphicon-pushpin'" @click=" element.fixed=! element.fixed" aria-hidden="true"></i>
                        @{{element.label}}
                        <a class="text-danger pull-right" v-on:click="removeItem1(index)"><i class="fa fa-trash-o"></i></a>
                      </li>
                    </transition-group>
                  </draggable>
                </div>
              </div>
              <hr>
            <div class="row">
              <div class="col-md-6">
                  <button type="submit" class="btn btn-primary" id="btn-submit"><i class="fa fa-save"></i> Save</button>
                  <a class="btn btn-default" href="{{ route('route.index') }}" role="button">Cancel</a>
              </div>
              <div class="col-md-6">
                  <button type="submit" class="btn btn-success btn-sm pull-right" id="btn-preview-on-map" ><i class="fa fa-map-marker"></i> Preview On Map</button>
              </div>
            </div>
            {!! Form::close() !!}

          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection

@push('styles')
<style>
  .flip-list-move {
    transition: transform 0.5s;
  }
  .no-move {
    transition: transform 0s;
  }
  .ghost {
    opacity: .5;
    background: #C8EBFB;
  }
  .list-group {
    min-height: 20px;
  }
  .list-group-item {
    cursor: move;
  }
  .list-group-item i{
    cursor: pointer;
  }
</style>
@endpush
@push('scripts')
<script>

$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


Vue.component('v-select', VueSelect.VueSelect);

const busstops = {!! $busstops !!};
const route_start_to_end = {!! $route_start_to_end !!};
const route_end_to_start = {!! $route_end_to_start !!};
var app = new Vue({
    el : '#app',
   data () {
      return {
        busstops: busstops,
        item:'',
        item1:'',
        list: route_start_to_end,
        list1: route_end_to_start,
        editable:true,
        isDragging: false,
        delayedDragging:false
      }
    },
    methods:{
      orderList () {
        this.list = this.list.sort((one,two) =>{return one.order-two.order; })
      },
      onMove ({relatedContext, draggedContext}) {
        const relatedElement = relatedContext.element;
        const draggedElement = draggedContext.element;
        return (!relatedElement || !relatedElement.fixed) && !draggedElement.fixed
      },
      addItem: function(event) {
        event.preventDefault();
        var newItem = this.getNewItem();
        if(! newItem.id) {
          alert('Please Select Bus Stop');
          return false;
        }
        var isExist = this.list.filter(function(item){
            if(item.id == newItem.id) return item.id;
        });
        if(! isExist.length){
          this.list.push(newItem);
          this.item = '';
        }else{
          alert('This Bus Stop already exist');
          this.item = '';
        }
      },
      addItem1: function(event) {
        event.preventDefault();
        var newItem = this.getNewItem1();
        if(! newItem.id) {
          alert('Please Select Bus Stop');
          return false;
        }
        var isExist = this.list1.filter(function(item){
            if(item.id == newItem.id) return item.id;
        });
        if(! isExist.length){
          this.list1.push(newItem);
          this.item1 = '';
        }else{
          alert('This Bus Stop already exist');
          this.item1 = '';
        }
      },
      getNewItem: function(){
        return {id: this.item.value , name: this.item.label,code: this.item.code, lat: this.item.lat , long: this.item.long, has_corner: this.item.has_corner ,order: true, listfixed: false, label: this.item.label};
      },
      getNewItem1: function(){
        return {id: this.item1.value , name: this.item1.label,code: this.item1.code, lat: this.item1.lat , long: this.item1.long, has_corner: this.item1.has_corner ,order: true, listfixed: false,label: this.item1.label};
      },
      removeItem : function (index) {
        //this.list.splice(this.list.indexOf(item), 1)
        this.list.splice(index, 1);
      },
      removeItem1 : function (index) {
        //this.list1.splice(this.list1.indexOf(item), 1)
        this.list1.splice(index, 1);
      },
    },
    computed: {
      dragOptions () {
        return  {
          animation: 0,
          group: 'description',
          disabled: !this.editable,
          ghostClass: 'ghost'
        };
      },
      listString(){
        return JSON.stringify(this.list, null, 2);
      }
    },
    watch: {
      isDragging (newValue) {
        if (newValue){
          this.delayedDragging= true
          return
        }
        this.$nextTick( () =>{
             this.delayedDragging =false
        })
      }
    }

});


$('#btn-submit').on('click', function(e) {
  e.preventDefault();
  var route_data = JSON.stringify(app.$data.list, null, 2);
  var route_data1 = JSON.stringify(app.$data.list1, null, 2);
  $('#route_start_to_end').val(route_data);
  $('#route_end_to_start').val(route_data1);
  $('#route-form').append("<input type='hidden' name='_method' value='PUT' />");
  $("#route-form").attr({"action":"{{ route('route.update',$bus) }}","target": "_self","method" : "POST"}).submit();
});

$('#btn-preview-on-map').on('click', function(e) {
  e.preventDefault();
  var route_data = JSON.stringify(app.$data.list, null, 2);
  var route_data1 = JSON.stringify(app.$data.list1, null, 2);
  $('#route_start_to_end').val(route_data);
  $('#route_end_to_start').val(route_data1);
   $("#route-form").submit();
});

</script>
@endpush
