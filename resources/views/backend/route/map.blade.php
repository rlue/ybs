<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Bus Route </title>
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
        float: left;
        width: 100%;
        height: 100%;
      }
    </style>
  </head>
  <body>
    <div id="map"></div>
    <script>


      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 12,
          center: {lat: 16.856873, lng: 96.156653},
          mapTypeId: 'terrain'
        });
        // init the polyline and set map
        var fPoly = new google.maps.Polyline({
            strokeColor: '#cb42f4',
            strokeOpacity: 1.0,
            strokeWeight:4,
            map:map
        });
        var rPoly = new google.maps.Polyline({
            strokeColor: '#3949AB',
            strokeOpacity: 1.0,
            strokeWeight:4,
            map:map
        });
        // Get the path form polyLine
        var fPath = fPoly.getPath();
        var rPath = rPoly.getPath();

        var froutes = {!! $data['froutes'] !!};
        var rroutes = {!! $data['rroutes'] !!};

        var infowindow = new google.maps.InfoWindow();
        var marker, i;
        var flightPlanCoordinates = [];
        for (i = 0; i < froutes.length; i++) {
          fPath.push(new google.maps.LatLng(froutes[i].lat, froutes[i].long));
          marker = new google.maps.Marker({
            position: new google.maps.LatLng(froutes[i].lat, froutes[i].long),
            map: map,
            label: froutes[i].name
          });

          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow.setContent(froutes[i].name);
              infowindow.open(map, marker);
            }
          })(marker, i));
        }
        //for rroutes
        for (i = 0; i < rroutes.length; i++) {
          rPath.push(new google.maps.LatLng(rroutes[i].lat, rroutes[i].long));
          marker = new google.maps.Marker({
            position: new google.maps.LatLng(rroutes[i].lat, rroutes[i].long),
            map: map,
            label: rroutes[i].name
          });

          google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
              infowindow.setContent(rroutes[i].name);
              infowindow.open(map, marker);
            }
          })(marker, i));
        }
      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAnuPCfATkGIl7rFif3WzdpiUHyTIvmpeA&callback=initMap">
    </script>
  </body>
</html>
