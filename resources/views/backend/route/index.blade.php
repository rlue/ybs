@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-bus"></i> Bus Route
            <ul class="nav nav-pills card-header-pills pull-right">
                <li class="nav-item ">
                    <a href="{{ route('route.export') }}" target="_blink" class="btn btn-success">
                      <i class="fa fa-file-excel-o" aria-hidden="true"></i> Export</a>
                </li>
              </ul>
          </div>
          <div class="card-block">
              <table class="table table-bordered table-striped table-condensed datatables">
                  <thead>
                      <tr>
                          <th width="5%">No</th>
                          <th>Bus</th>
                          <th>Map</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach ($routes as $key => $route)
                        <tr>
                          <td> {{ ++$key }}</td>
                          <td>{{ $route->name }}</td>
                          <td >
                            {!! Form::open(['route' => ['route.map'],'method' => 'POST','class' => 'form-horizontal','id' => 'route-form','target' => '_blank']) !!}
                            <input type="hidden" name="bus_id" value="{{ $route->id}}">
                            <button type="submit" class="btn btn-success btn-sm pull-right"><i class="fa fa-map-marker"></i> View On Map</button>
                            {!! Form::close() !!}
                          </td>
                          <td>
                            <a href="{{ route('route.edit',['route' => $route]) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                          </td>
                        </tr>
                      @endforeach
                  </tbody>
              </table>
              <nav>

              </nav>
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection
