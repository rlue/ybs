@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-bus"></i> Bus In/Out
              <div class="pull-right">
                  
              </div>
          </div>
          <div class="card-block">
              <table class="table table-bordered table-striped table-condensed datatables" id="township-table">
                  <thead>
                      <tr>
                          <th width="5%">No</th>
                          <th>Service No</th>
                          <th>Gate User Name</th>
                          <th>Gate Name</th>
                          <th>Bus Number</th>
                          <th>In/Out</th>
                          <th>Time</th>
                          <th>Date</th>
                      </tr>
                  </thead>
                  <tbody>

                      @foreach ($register as $key => $gate)
                        <tr>
                          <td> {{ ++$key }}</td>
                          <td>{{ $gate->bus->name}}</td>
                           <td>{{ $gate->user->name}}</td>
                            <td>{{ $gate->gate->gate_name}}</td>
                            <td>{{$gate->gateRegister_busNo}}</td>
                            <td>@if($gate->gateRegister_busNo == 1)
                              In
                            @else
                             Out
                            @endif
                            </td>
                            <td>{{$gate->gateRegister_time}}</td>
                            <td>{{$gate->gateRegister_date}}</td>
                          
                        </tr>
                      @endforeach
                  </tbody>
              </table>
              <nav>
                 {{--  {!! $gates->links('vendor.pagination.bootstrap-4') !!} --}}
              </nav>
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection

