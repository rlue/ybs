@extends('backend.layout')

@section('content')
      <div class="card">
        <div class="card-header">
          Create Role
        </div>
        <div class="card-block">
            {!! Form::open(['route' => 'role.store','method' => 'POST']) !!}
               <div class="row">
                 <div class="col-md-6">
                   <div class="form-group">
                      {!! Form::text('name','',['class' => 'form-control ','placeholder' => 'Enter Role Name']) !!}
                   </div>
                 </div>
               </div>
               <div class="row">
{{--                 <div class="col-md-12 form-group">
                   @foreach (config('backend.permissions') as $key => $permission)
                        <h5>{{ ucwords($key) }} Access </h5>
                        <div class="row">
                          @foreach ($permission as $key)
                              <div class="col-sm-3">
                                <div class="checkbox-custom checkbox-success">
                                   <input class="permission-name" type="checkbox" id="{{ $key }}" value="{{ $key }}" name="permissions[]" >
                                  <label for="{{$key}}">{{ $key }}</label>
                                </div>
                              </div>
                          @endforeach
                        </div>
                   @endforeach
                </div> --}}
               </div>
             <button type="submit" class="btn btn-success">Save</button>
             <a class="btn btn-default" href="{{ route('role.index') }}" role="button">Cancel</a>
          {!! Form::close() !!}
        </div>
      </div>
@endsection
