@extends('backend.layout')
@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-align-justify"></i> Roles
            <div class="pull-right">
              <a href="{{ route('role.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
            </div>
          </div>
          <div class="card-block">
              <table class="table table-bordered table-striped table-condensed">
                  <thead>
                      <tr>
                          <th>Name</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach ($roles as $role)
                        <tr>
                          <td>{{ $role->name}}</td>
                          <td>
                            @can('update_role')
                            <a href="{{ route('role.edit',$role) }}" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i> Edit</a>
                            @endcan
                            @can('delete_role')                            
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => route('role.destroy',$role),
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-sm',
                                        'title' => 'Delete',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                            @endcan
                          </td>
                        </tr>
                      @endforeach
                  </tbody>
              </table>
              <nav>
                  {!! $roles->links() !!}
              </nav>
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection
