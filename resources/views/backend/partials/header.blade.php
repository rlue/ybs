<header class="app-header navbar">
  <button class="navbar-toggler mobile-sidebar-toggler d-lg-none" type="button">☰</button>
  <a class="navbar-brand" href="#"></a>
  <ul class="nav navbar-nav d-md-down-none">
    <li class="nav-item">
      <a class="nav-link navbar-toggler sidebar-toggler" href="#">☰</a>
    </li>

{{--     <li class="nav-item px-3">
      <a class="nav-link" href="#">Dashboard</a>
    </li>
    <li class="nav-item px-3">
      <a class="nav-link" href="#">Users</a>
    </li>
    <li class="nav-item px-3">
      <a class="nav-link" href="#">Settings</a>
    </li> --}}
  </ul>
  <ul class="nav navbar-nav ml-auto">
    <li class="nav-item dropdown">
      <a class="nav-link dropdown-toggle nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
       {{--  <img src="img/avatars/6.jpg" class="img-avatar" alt="admin@bootstrapmaster.com"> --}}
        <span class="d-md-down-none">{{ auth()->user()->name }}</span>
      </a>
      <div class="dropdown-menu dropdown-menu-right">

        <div class="dropdown-header text-center">
          <strong>Account</strong>
        </div>
        <a class="dropdown-item" href="{{ route('user.profile') }}"><i class="fa fa-user-o" aria-hidden="true"></i>Profile</a>
        <a class="dropdown-item" href="{{ route('admin.logout') }}"><i class="fa fa-lock"></i> Logout</a>
      </div>
    </li>
    <li class="nav-item d-md-down-none">
      
    </li>

  </ul>
</header>
