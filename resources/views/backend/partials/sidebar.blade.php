<div class="sidebar">
  <nav class="sidebar-nav">
    <ul class="nav">
      <li class="nav-item active">
        <a class="nav-link" href="{{ route('admin.home') }}"><i class="icon-speedometer"></i> Dashboard </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('bus.index') }}" target="_top"><i class="fa fa-bus" aria-hidden="true"></i></i> Bus</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('busstop.index') }}" target="_top"><i class="fa fa-map-marker" aria-hidden="true"></i>Bus Stops</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('route.index') }}" target="_top"><i class="fa fa-map-o" aria-hidden="true"></i>Route</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('township.index') }}" target="_top"><i class="fa fa-home" aria-hidden="true"></i>Township</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('street.index') }}" target="_top"><i class="fa fa-road" aria-hidden="true"></i>Street</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('gateregister.index') }}" target="_top"><i class="fa fa-road" aria-hidden="true"></i>Bus In/Out</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('busstate.index') }}" target="_top"><i class="fa fa-road" aria-hidden="true"></i>Bus State</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('gatekeeper.index') }}" target="_top"><i class="fa fa-road" aria-hidden="true"></i>Gate Keeper</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('gate.index') }}" target="_top"><i class="fa fa-road" aria-hidden="true"></i>Gate</a>
      </li>
      @role('admin')
      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#"><i class="fa fa-universal-access"></i> Administrators</a>
        <ul class="nav-dropdown-items">
          <li class="nav-item">
            <a class="nav-link" href="{{ route('user.index') }}" target="_top"><i class="fa fa-users"></i> Users</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{ route('role.index') }}" target="_top"><i class="icon-star"></i> Roles</a>
          </li>
          {{--
          <li class="nav-item">
            <a class="nav-link" href="{{ route('permission.index') }}" target="_top"><i class="fa fa-snowflake-o"></i> Permissions</a>
          </li> --}}
        </ul>
      </li>
      @endrole
    </ul>
  </nav>
</div>
