<!DOCTYPE html>
<html class='no-js' lang=''>
<head>
  <meta charset='utf-8'>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta content='{{ csrf_token() }}' name='_token'>
  <title>@yield('title', 'Spring ')</title>
  <meta content='@yield('description', '')' name='description'>
  <meta content='@yield('author', '')' name='author'>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  @yield('meta')

  @yield('before-styles')

  {!! css([
    'bower_components/fontawesome/css/font-awesome.min',
    'bower_components/simple-line-icons/css/simple-line-icons',
    'bower_components/select2/dist/css/select2.min',
    "plugins/bootstrap3-editable/bootstrap3-editable/css/bootstrap-editable",
    'css/style'
  ], 'assets/backend/') !!}

  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap4.min.css">
  @stack('styles')

</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed aside-menu-hidden">

@include('backend.partials.header')

<div class="app-body">
  @include('backend.partials.sidebar')
  <main class="main">
    @include('backend.partials.breadcrumb')
    <div class="container-fluid">
      @include('flash::message')
      @include('backend.partials.errors')
      @yield('content')
    </div>
  </main>
</div>
@include('backend.partials.footer')

<!-- Bootstrap and necessary plugins -->
{!! js([
  "jquery/dist/jquery.min",
  "tether/dist/js/tether.min",
  "bootstrap/dist/js/bootstrap.min",
  "pace/pace.min"
], 'assets/backend/bower_components/') !!}
<!-- Plugins and scripts required by all views -->
{!! js([
  "chart.js/dist/Chart.min",
  'select2/dist/js/select2.min'
], 'assets/backend/bower_components/') !!}
<!-- GenesisUI main scripts -->
{!! js([
"js/coreui",
"plugins/bootstrap3-editable/bootstrap3-editable/js/bootstrap-editable"
], 'assets/backend/') !!}

<script src="https://unpkg.com/vue@2.4.2"></script>
<!-- CDNJS :: Sortable (https://cdnjs.com/) -->
<script src="//cdnjs.cloudflare.com/ajax/libs/Sortable/1.6.0/Sortable.min.js"></script>

<!-- CDNJS :: Vue.Draggable (https://cdnjs.com/) -->
<script src="//cdnjs.cloudflare.com/ajax/libs/Vue.Draggable/2.14.1/vuedraggable.min.js"></script>
<script src="https://unpkg.com/vue-select@latest"></script>
<script src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.15/js/dataTables.bootstrap4.min.js"></script>
 @stack('scripts')

 <script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
     $('.datatables').DataTable({});
 </script>
</body>
</html>
