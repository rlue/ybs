@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-bus"></i> Gates Keeper
              <div class="pull-right">
                  <a href="{{ route('gatekeeper.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
              </div>
          </div>
          <div class="card-block">
              <table class="table table-bordered table-striped table-condensed datatables" id="township-table">
                  <thead>
                      <tr>
                          <th width="5%">No</th>
                          <th>Name</th>
                          <th>Gate Name</th>
                          <th>Service No</th>
                          <th>Account Name</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>

                      @foreach ($keeper as $key => $gate)
                        <tr>
                          <td> {{ ++$key }}</td>
                          <td>{{ $gate->gatekeeper_name}}</td>
                          <td>{{ $gate->busgate->gate_name}}</td>
                           <td>{{ $gate->bus->name}}</td>
                            <td>{{ $gate->gatekeeper_accountName}}</td>
                          <td>
                            <a href="{{ route('gatekeeper.edit',$gate->gatekeeper_id) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                             <a href="{{ route('gatekeeper.destroy',$gate->gatekeeper_id) }}" class="btn btn-danger btn-sm"><i class="fa fa-remove"></i> Delete</a>
                          </td>
                        </tr>
                      @endforeach
                  </tbody>
              </table>
              <nav>
                 {{--  {!! $gates->links('vendor.pagination.bootstrap-4') !!} --}}
              </nav>
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection

