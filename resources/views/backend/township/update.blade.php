@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-align-justify"></i> Update Township
          </div>
          <div class="card-block">
         {!! Form::open(['route' => (['township.update',$township]),'method' => 'PUT','class' => 'form-2orizontal','autocomplete' => 'false']) !!}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="name" class="form-label">Name</label>
                   <input type="text" name="name" class="form-control" value="{{ $township->name }}" required="required" placeholder="Enter Township Name">
                  </div>
                </div>
              </div>
              <hr>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
            <a class="btn btn-default" href="{{ route('township.index') }}" role="button">Cancel</a>
            {!! Form::close() !!}
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection



@push('scripts')
<script>

/* $('#bus-stops').select2({
    tags: true
 });

$("#bus-stops").on("select2:select", function (evt) {
  var element = evt.params.data.element;
  var $element = $(element);

  $element.detach();
  $(this).append($element);
  $(this).trigger("change");
});*/

</script>
@endpush
