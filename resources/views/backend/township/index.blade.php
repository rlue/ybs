@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-bus"></i> Townships
              <div class="pull-right">
                  <a href="{{ route('township.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
              </div>
          </div>
          <div class="card-block">
              <table class="table table-bordered table-striped table-condensed datatables" id="township-table">
                  <thead>
                      <tr>
                          <th width="5%">No</th>
                          <th>Name</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach ($townships as $key => $township)
                        <tr>
                          <td> {{ ++$key }}</td>
                          <td>{{ $township->name}}</td>
                          <td>
                            <a href="{{ route('township.edit',['bus' => $township]) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => route('township.destroy',['township' => $township]),
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-sm',
                                        'title' => 'Delete',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                          </td>
                        </tr>
                      @endforeach
                  </tbody>
              </table>
              <nav>
                 {{--  {!! $townships->links('vendor.pagination.bootstrap-4') !!} --}}
              </nav>
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection

