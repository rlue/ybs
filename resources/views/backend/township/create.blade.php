@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-align-justify"></i> Create Township
          </div>
          <div class="card-block">
            {!! Form::open(['route' => 'township.store','method' => 'POST','class' => 'form-2orizontal']) !!}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="name" class="form-label">Township Name</label>
                   <input type="text" name="name" class="form-control" value="{{old('name')}}" required="required" placeholder="Enter Township Name">
                  </div>
                </div>
              </div>
              <hr>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a class="btn btn-default" href="{{ route('township.index') }}" role="button">Cancel</a>
            {!! Form::close() !!}
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection


@push('scripts')
<script>
</script>
@endpush
