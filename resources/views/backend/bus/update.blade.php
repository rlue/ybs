@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-align-justify"></i> Update Bus
          </div>
          <div class="card-block">
         {!! Form::open(['route' => (['bus.update',$bus]),'method' => 'PUT','class' => 'form-2orizontal','autocomplete' => 'false']) !!}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="name" class="form-label">Name</label>
                   <input type="text" name="name" class="form-control" value="{{ $bus->name }}" required="required" placeholder="Enter Bus Name">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="operator" class="form-label">Operator</label>
                   <input type="text" name="operator" class="form-control" value="{{ $bus->operator }}" placeholder="Enter Operator">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="password" class="form-label">Password</label>
                   <input type="text" name="password" id="password" class="form-control" autocomplete="false" value="{{ $bus->password }}" placeholder="Enter Password">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="user" class="form-label">User Nmae</label>
                   <input type="text" name="user" class="form-control" value="{{ $bus->user }}" required="required" placeholder="Enter User Name">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="server" class="form-label">Server</label>
                   <select name="server" class="form-control" required="required">
                     <option value="">Select Server</option>
                     <option value="yangonbus.selfip.net" @if($bus->server == 'yangonbus.selfip.net') selected @endif >yangonbus.selfip.net</option>
                     <option value="yangonbus2.selfip.net" @if($bus->server == 'yangonbus2.selfip.net') selected @endif>yangonbus2.selfip.net</option>
                   </select>
                  </div>
                </div>
              </div>
              <hr>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
            <a class="btn btn-default" href="{{ route('bus.index') }}" role="button">Cancel</a>
            {!! Form::close() !!}
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection



@push('scripts')
<script>

/* $('#bus-stops').select2({
    tags: true
 });

$("#bus-stops").on("select2:select", function (evt) {
  var element = evt.params.data.element;
  var $element = $(element);

  $element.detach();
  $(this).append($element);
  $(this).trigger("change");
});*/

</script>
@endpush
