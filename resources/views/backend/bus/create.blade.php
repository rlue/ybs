@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-align-justify"></i> Create Bus
          </div>
          <div class="card-block">
            {!! Form::open(['route' => 'bus.store','method' => 'POST','class' => 'form-2orizontal']) !!}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="name" class="form-label">Bus Name</label>
                   <input type="text" name="name" class="form-control" value="{{old('name')}}" required="required" placeholder="Enter Bus Name">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="operator" class="form-label">Operator</label>
                   <input type="text" name="operator" class="form-control" value="{{ old('operator') }}"  placeholder="Enter Operator">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="password" class="form-label">Password</label>
                   <input type="text" name="password" id="password" class="form-control" value="{{ old('password') }}" placeholder="Enter Password">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="user" class="form-label">User Name</label>
                   <input type="text" name="user" class="form-control" value="{{ old('user') }}" required="required" placeholder="Enter User Name">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="server" class="form-label">Server</label>
                   <select name="server" class="form-control" required="required">
                     <option value="">Select Server</option>
                     <option value="yangonbus.selfip.net">yangonbus.selfip.net</option>
                     <option value="yangonbus2.selfip.net">yangonbus2.selfip.net</option>
                   </select>
                  </div>
                </div>
              </div>
              <hr>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
            <a class="btn btn-default" href="{{ route('bus.index') }}" role="button">Cancel</a>
            {!! Form::close() !!}
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection


@push('scripts')
<script>
</script>
@endpush
