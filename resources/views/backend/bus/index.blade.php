@extends('backend.layout')

@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-bus"></i> Bus
              <div class="pull-right">
                  <a href="{{ route('bus.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
              </div>
          </div>
          <div class="card-block">
              <table class="table table-bordered table-striped table-condensed datatables">
                  <thead>
                      <tr>
                          <th width="5%">No</th>
                          <th>Name</th>
                          <th>User</th>
                          <th>Server</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                      @foreach ($buses as $key => $bus)
                        <tr>
                          <td> {{ ++$key }}</td>
                          <td>{{ $bus->name}}</td>
                          <td>{{ $bus->user}}</td>
                          <td>{{ $bus->server}}</td>
                          <td>
                            <a href="{{ route('bus.edit',['id' => $bus->id]) }}" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => route('bus.destroy',['bus' => $bus]),
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-sm',
                                        'title' => 'Delete',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                          </td>
                        </tr>
                      @endforeach
                  </tbody>
              </table>
              <nav>

              </nav>
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection
