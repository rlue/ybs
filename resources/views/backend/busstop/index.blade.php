@extends('backend.layout')

@section('content')

<div class="row">
  <div class="col-lg-12">
      <div class="success-alert-area"></div>
      <div class="card">
          <div class="card-header">
            <i class="fa fa-align-justify"></i> <a href="#" id="code-edit">Bus Stops</a>
            <ul class="nav nav-pills card-header-pills pull-right">
                <li class="nav-item">
                    <a href="{{ route('busstop.create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> Add</a>
                </li>
                <li class="nav-item ">
                    <a href="{{ route('busstop.export') }}" target="_blink" class="btn btn-success">
                      <i class="fa fa-file-excel-o" aria-hidden="true"></i> Export</a>
                </li>
              </ul>
          </div>
          <div class="card-block">
              <table class="table table-striped table-bordered" id="bus-table">
                  <thead>
                      <tr>
                          <th width="5%">No</th>
                          <th>Name</th>
                          <th>Code</th>
                          <th>Street</th>
                          <th>Township</th>
                          <th>Lat</th>
                          <th>Long</th>
                          <th>Action</th>
                      </tr>
                  </thead>
                  <tbody>
                  </tbody>
              </table>
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection
@push('scripts')
<script>
$(function() {
  var baseUrl = '{!! URL::to("/") !!}';

  var tbl = $('#bus-table').DataTable({
    processing: true,
    serverSide: true,
    iDisplayLength: 25,
    'responsive': true,
    'language' : {
      "sSearchPlaceholder": "Search..",
      "lengthMenu": "_MENU_",
      "search": "_INPUT_"
    },
    order: [[ 1, "asc" ]],
    ajax: {
      url  :  baseUrl + '/admin/busstop/data',
      type : "POST",
      data : {}
    },
    columns: [
      { data: 'no', orderable: false, bSearchable: false },
      { data: 'name', name: 'name' },
      { data: 'code', name: 'code' },
      { data: 'street', name: 'street.name'},
      { data: 'township', name: 'township.name'},
      { data: 'lat', name: 'lat'},
      { data: 'long', name: 'long'},
      { data: 'action', name: 'action', 'bSortable': false, 'searchable': false}
    ],
    "fnRowCallback" : function(nRow, aData, iDisplayIndex){
        // For auto numbering at 'No' column
        var start = tbl.page.info().start;
        $('td:eq(0)',nRow).html(start + iDisplayIndex + 1);
    }
  });


  $('#bus-table').on('click','.del-link', function(e){
    e.preventDefault();
    var id= $(this).data('id');
    var del = confirm('Are you sure to delete?');
    if(del){
      $.ajax({
          type :  "DELETE",
          url  :  baseUrl + '/admin/busstop/' + id,
          data :  {},
          success: function(data){
              var result = $.parseJSON(data);
              if(result['status'] == 'success') {
                  $(".success-alert-area").empty().append("<div class='alert alert-success success-display'><a href='#' class='close' data-dismiss='alert'>&times;</a>"+result['msg']+"</div>");
                    tbl.ajax.reload( null, false );
              }
              else {
                alert(result['msg']);
              }
          },
          error: function(XMLHttpRequest, textStatus, errorThrown) {
              alert("ERROR!!!");
              alert(errorThrown);
          }
      });
    }
  });

  //inline edit
  $.fn.editable.defaults.mode = 'inline';

  $('#bus-table').on('click','.code-edit,.name-edit,.lat-edit,.long-edit', function(e){
     e.preventDefault();
     $(this).editable({
        success: function(response, newValue) {
            //tbl.ajax.reload( null, false );
        }
     })
    
  });


  function getTownships() {
    var url = "/admin/township/list";
    return $.ajax({
        type:  'GET',
        async: true,
        url:   url,
        dataType: "json"
    });
  };

  getTownships().done(function(result) {

    $('#bus-table').on('click','.township-edit', function(e){
       e.preventDefault();
       var id = $(this).data('township-id');
       $(this).editable({
          value: id,    
          source: result,
       });
    });

  });



  function getStreets() {
    var url = "/admin/street/list";
    return $.ajax({
        type:  'GET',
        async: true,
        url:   url,
        dataType: "json"
    });
  };

  getStreets().done(function(result) {

    $('#bus-table').on('click','.street-edit', function(e){
       e.preventDefault();
       var id = $(this).data('street-id');
       $(this).editable({
          value: id,    
          source: result,
       });
    });

  });


});
  </script>
@endpush

