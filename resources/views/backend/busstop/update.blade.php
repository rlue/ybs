@extends('backend.layout')

@section('content')

<div class="modal fade show" id="pickLocation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Pick Location</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                  <div class="form-group col-md-4">
                    <label class="form-label">Lat</label>
                    <input type="text" name="pick-lat" id="pick-lat" class="form-control" value="" >
                  </div>
                  <div class="form-group col-md-4">
                    <label class="form-label">Long</label>
                    <input type="text" name="pick-long" id="pick-long" class="form-control" value="" >
                  </div>
                </div>
                <div id="locationPicker" style="width: 750px; height: 400px;"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" id="btn-add-location" class="btn btn-primary">Add</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="row">
  <div class="col-lg-12">
      <div class="card">
          <div class="card-header">
              <i class="fa fa-align-justify"></i> Update BusStop
          </div>
          <div class="card-block">
         {!! Form::open(['route' => (['busstop.update',$busstop]),'method' => 'PUT','class' => 'form-2orizontal','autocomplete' => 'false']) !!}
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="name" class="form-label">Name</label>
                   <input type="text" name="name" class="form-control" value="{{ $busstop->name }}" required="required" placeholder="Enter BusStop Name">
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="code" class="form-label">Code</label>
                   <input type="text" name="code" class="form-control" value="{{ $busstop->code }}" required="required" placeholder="Enter Bus Stop Code">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-2">
                  <div class="form-group">
                   <label for="lat" class="form-label">Lat  </label>
                   <input type="text" name="lat" id="lat" class="form-control" value="{{ $busstop->lat }}" required="required" placeholder="Enter Latitude">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group">
                   <label for="long" class="form-label">Long  </label>
                   <input type="text" name="long" id="long" class="form-control" value="{{ $busstop->long }}" required="required" placeholder="Enter Longitude">
                  </div>
                </div>
                <div class="col-md-2">
                  <div class="form-group" style="margin-top: 30px">
                    <button type="button" id="pick-location" class="btn btn-primary">
                      <i class="fa fa-map-marker"> </i>
                    Pick</button>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="street" class="form-label">Street</label>
                   {!! Form::select('street_id',$streets,$busstop->street_id,['class' => 'form-control select2','placeholder' => 'Select Street']) !!}
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                   <label for="township" class="form-label">Township</label>
                   {!! Form::select('township_id',$townships,$busstop->township_id,['class' => 'form-control select2','placeholder' => 'Select Township']) !!}
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group"></div>
                  <div class="form-group">
                    <label class="custom-control custom-checkbox">
                      <input type="checkbox" class="custom-control-input" name="has_corner" {{ ($busstop->has_corner) ? 'checked' : ''}}>
                      <span class="custom-control-indicator"></span>
                      <span class="custom-control-description">Junction</span>
                    </label>
                  </div>
                </div>
              </div>
            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
            <a class="btn btn-default" href="{{ route('busstop.index') }}" role="button">Cancel</a>
            {!! Form::close() !!}
          </div>
      </div>
  </div>
  <!--/.col-->
</div>
@endsection



@push('scripts')
<script  src='http://maps.google.com/maps/api/js?key=AIzaSyAnuPCfATkGIl7rFif3WzdpiUHyTIvmpeA&libraries=places'></script>

<script src="{{ asset('assets/backend/js/jquery.locatinpicker.js') }} "></script>
<script>

 $('.select2').select2({
 });


$('#locationPicker').locationpicker({
    location: {
          latitude: $('#lat').val(),
          longitude: $('#long').val()
    },
    radius: 300,
    inputBinding: {
        latitudeInput: $('#pick-lat'),
        longitudeInput: $('#pick-long'),
    },
    enableAutocomplete: true,
});
$('#pickLocation').on('shown.bs.modal', function () {
    $('#locationPicker').locationpicker('autosize');
});

 $('#pick-location').on('click', function(e) {
    e.preventDefault();
    $('#pick-lat').val($('#lat').val());
    $('#pick-long').val($('#long').val());
    $('#pickLocation').modal('show');
 });

 $('#btn-add-location').on('click', function(e) {
  e.preventDefault();
    $('#lat').val($('#pick-lat').val());
    $('#long').val($('#pick-long').val());
    $('#pickLocation').modal('hide');
  });
</script>
@endpush
