<?php

  Route::group(['namespace' => '\\Backend\\Auth'], function() {

      Route::get('login', 'LoginController@showLoginForm')->name('admin.showLogin');
      Route::post('login', 'LoginController@login')->name('admin.postLogin');
      Route::get('logout', 'LoginController@logout')->name('admin.logout')->middleware('admin.auth' );

  });

  Route::group(['middleware' => 'admin.auth','namespace' => 'Backend'], function() {
      Route::get('/', [
          'as'   => 'admin.home',
          'uses' => 'DashboardController@index'
      ]);
      Route::get('user/profile','UserController@profile')->name('user.profile');
      Route::put('user/profile/{user}','UserController@updateProfile')->name('user.updateProfile');
      Route::resource('user','UserController');
      Route::resource('permission','PermissionController');
      Route::resource('role','RoleController');

      Route::resource('bus','BusController');
      Route::get('busstop/export','BusStopController@getExportExcel')->name('busstop.export');
      Route::post('busstop/quickUpdate','BusStopController@quickUpdate')->name('busstop.quickUpdate');
      Route::post('busstop/data','BusStopController@data');
      Route::resource('busstop','BusStopController');

      Route::get('route/export','BusRouteController@getExportExcel')->name('route.export');
      Route::post('route/map','BusRouteController@getMap')->name('route.map');

      Route::resource('route','BusRouteController');

      Route::get('township/list','TownshipController@getTownshipList');
      Route::resource('township','TownshipController');

      Route::get('gate/list','BusGateController@getGateList');
      Route::resource('gate','BusGateController');

      Route::get('gatekeeper/list','GateKeeperController@getGateKeeperList');
      Route::resource('gatekeeper','GateKeeperController');

      Route::get('gateregister/list','GateRegisterController@getGateRegisterList');
      Route::resource('gateregister','GateRegisterController');

      Route::get('busstate/list','BusStateController@getBusStateList');
      Route::resource('busstate','BusStateController');

      Route::get('street/list','StreetController@getStreetList');
      Route::resource('street','StreetController');
  });



