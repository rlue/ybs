<?php
use Illuminate\Http\Request;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Backend\\Api'], function() {

    Route::post('/bus-stops','BusStopApiController@index');
    Route::post('/bus','BusApiController@index');
    Route::post('/getUpdatedTime','BusApiController@getUpdatedTime');
    Route::post('/busRoutes','BusApiController@getBusRoutes');

    Route::post('/getVehicles','VehiclesApiController@index');
    Route::post('/getDeviceStatus','DeviceApiController@index');
    Route::post('/getDevices','SdeviceApiController@index');

    Route::post('/busRegisterGate','GateRegisterApiController@BusRegisterGate');
    Route::post('/getGateUser','GateRegisterApiController@getGateUser');
    Route::post('/getGate','GateRegisterApiController@getGate');
    Route::post('/busState','GateRegisterApiController@busState');
    Route::post('/getBusRegisterGate','GateRegisterApiController@getBusRegisterGate');
    Route::post('/getBusState','GateRegisterApiController@getBusState');
});

