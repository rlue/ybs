<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateBusStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bus_states', function (Blueprint $table) {
            $table->increments('busState_id');
            $table->integer('busState_serviceno');
            $table->tinyInteger('busState_onoff');
            $table->string('busState_busno');
            $table->string('busState_time');
            $table->date('busState_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bus_states');
    }
}
