<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFlagsToBusStopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bus_stops', function (Blueprint $table) {
            $table->integer('road')->change();
            $table->renameColumn('road', 'street_id');
            $table->integer('township')->change();
            $table->renameColumn('township', 'township_id');
            $table->string('code')->after('name');
            $table->boolean('has_corner')->default(0)->after('long');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bus_stops', function (Blueprint $table) {
            //
        });
    }
}
