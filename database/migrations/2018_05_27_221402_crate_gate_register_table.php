<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CrateGateRegisterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gate_register', function (Blueprint $table) {
            $table->increments('gateRegister_id');
            $table->integer('gateRegister_serviceNo');
            $table->integer('gateRegister_userid');
            $table->integer('gateRegister_gateid');
            $table->string('gateRegister_time');
            $table->tinyInteger('gateRegister_inout');
            $table->string('gateRegister_busNo');
            $table->date('gateRegister_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gate_register');
    }
}
