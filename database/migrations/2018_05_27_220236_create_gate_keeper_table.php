<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGateKeeperTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gate_keeper', function (Blueprint $table) {
            $table->increments('gatekeeper_id');
            $table->string('gatekeeper_name');
            $table->integer('service_number');
            $table->string('gatekeeper_password');
            $table->string('gatekeeper_accountName');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gate_keeper');
    }
}
