<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGatekeeperGateidToGateKeeperTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gate_keeper', function (Blueprint $table) {
            $table->integer('gatekeeper_gateid');
            //
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gate_keeper', function (Blueprint $table) {
            //
            $table->dropColumn(['gatekeeper_gateid']);
        });
    }
}
