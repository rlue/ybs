<?php

use Illuminate\Database\Seeder;
use App\BusStop;

class BusStopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = file_get_contents(base_path('database/data/bus_stop.json'));
        
        $busstops = json_decode($data);
        foreach ($busstops as $key => $value) {
           BusStop::create(['name' => $value->name,
                            'label'=> $value->label,
                            'street_id' => 1,
                            'township_id' => 1,
                            'code' => rand(),
                            'lat'      => $value->geo[1],
                            'long'     => $value->geo[0],
                          ]);
         } 

    }
}
