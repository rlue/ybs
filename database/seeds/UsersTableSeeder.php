<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;
use App\Permission;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name'    => 'admin',
            'email'   => 'admin@gmail.com',
            'password' => bcrypt('admin'),
        ]);

        $role = Role::create([
            'name'  => 'admin'
          ]);

        $user->assignRole('admin');


        $permissions = array_flatten(config('backend.permissions'));

        foreach ($permissions as $value) {
           Permission::firstOrCreate(['name' => $value]);
        }

        $permissions = Permission::pluck('name')->toArray();

        $role->givePermissionTo($permissions);
    }
}
