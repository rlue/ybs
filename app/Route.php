<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Bus;

class Route extends Model
{
    protected $fillable = ['bus_id','routes'];

    protected $casts = [
        'routes' => 'json',
    ];

    public function bus()
    {
      return $this->belongsTo(Bus::class);
    }
}
