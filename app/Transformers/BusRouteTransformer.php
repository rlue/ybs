<?php namespace App\Transformers;

/**
* BusStop Api Transformer
*/
class BusRouteTransformer extends Transformer
{
  public function transform($item)
  {
    return [
      'id'      =>  $item['id'],
      'name'    => $item['name'],
      'code'    => $item['code'],
      'lat'     => $item['lat'],
      'long'     => $item['long'],
      'has_corner' => $item['has_corner']
    ];
  }
}
