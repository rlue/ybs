<?php namespace App\Transformers;

use App\Bus;
/**
* BusStop Api Transformer
*/
class BusTransformer extends Transformer
{
  public function transform($item)
  { 
    $bus = Bus::find($item->id);
    $fArr = $bus->busstops()->wherePivot('type','F')->orderBy('pivot_order', 'asc')->get()->toArray();
    $start_to_end = array_first($fArr)['name'] . ' - ' .  array_last($fArr)['name'];

    $rArr = $bus->busstops()->wherePivot('type','R')->orderBy('pivot_order', 'asc')->get()->toArray();
    $end_to_start = array_first($rArr)['name'] . ' - ' .  array_last($rArr)['name'];
   
    return [
      'name'  => $item->name,
      'operator'  => $item->operator,
      'user'  => $item->user,
      'password' => $item->password,
      'server'   => $item->server,
      'start_to_end' => $start_to_end,
      'end_to_start' => $end_to_start
    ];
  }
}
