<?php namespace App\Transformers;

/**
* BusStop Api Transformer
*/
class BusStopTransformer extends Transformer
{
  public function transform($item)
  {
    $bus = $item->bus->pluck('name')->toArray();
    return [
      'name'  => $item->name,
      'code'  => $item->code,
      'street'  => isset($item->street->name) ? $item->street->name : '',
      'township' => isset($item->township->name) ? $item->township->name : '',
      'lat'     => $item->lat,
      'long'    => $item->long,
      'has_corner' => $item->has_corner,
      'bus'     => $bus
    ];
  }
}
