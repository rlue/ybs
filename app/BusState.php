<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusState extends Model
{
    //
     protected $primaryKey = 'busState_id';

    protected $fillable = ['busState_serviceno','busState_onoff','busState_busno','busState_time','busState_date'];

    public function bus()
    {
        return $this->belongsTo(Bus::class,'busState_serviceno');
    }
    
}
