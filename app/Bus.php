<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\BusStop;
class Bus extends Model
{
    protected $table = 'bus';

    protected $fillable = ['name','operator','user','password','server','route_start_to_end','route_end_to_start','updated_at'];


    public function getUserName()
    {
      return $this->user;
    }

    public function getPassword()
    {
      return $this->password;
    }

    public function busstops()
    {
        return $this->belongsToMany(BusStop::class,'bus_busstop','bus_id','busstop_id')->withPivot('type','order');
    }

}
