<?php

namespace App\Http\Middleware;

use Closure;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class HostFilter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (env('APP_ENV') == 'local') {
            return $next($request);
        }
        
        if (in_array($request->getHost(), config('backend.allow_hosts'))) {
            return $next($request);
        }

        throw new BadRequestHttpException("Now Allow! :P");
    }
}
