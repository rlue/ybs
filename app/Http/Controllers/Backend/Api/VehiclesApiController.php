<?php namespace App\Http\Controllers\Backend\Api;


use Illuminate\Http\Request;
use App\Bus;

class VehiclesApiController extends GuzzleClient
{
    protected $api_url = 'StandardApiAction_queryUserVehicle.action?';


    function __construct(Request $request)
    {
      parent::__construct();
      $user = $request->get('user');
      $password = $request->get('password');
      if ($user && $password) {
        $this->login($user,$password);
      }else{
        $ret = ['status' => 'fail','msg' => 'Unauthorize access.Please check user name and password!'];
        echo json_encode($ret);
      }
    }

    public function index()
    {
      $response = $this->client->get($this->api_url,['query' => [
          'jsession'  => $this->jsession
        ]]);
      $data = $response->getBody();

      echo $data;
    }
}
