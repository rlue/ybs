<?php namespace App\Http\Controllers\Backend\Api;


use Illuminate\Http\Request;

class DeviceApiController extends GuzzleClient
{
    protected $api_url = 'StandardApiAction_getDeviceStatus.action?';

    protected $vehiIdno;

    function __construct(Request $request)
    {
      parent::__construct();
      parent::__construct();
      $user = $request->get('user');
      $password = $request->get('password');
      if ($user && $password) {
        $this->login($user,$password);
      }else{
        $ret = ['status' => 'fail','msg' => 'Unauthorize access.Please check user name and password!'];
        echo json_encode($ret);
      }
      $this->vehiIdno = $request->get('vehiIdno');
      if ( empty($this->vehiIdno)) {
          $ret = ['status' => 'fail','msg' => 'Unauthorize access.vehiIdno must include in request'];
          echo json_encode($ret);  
      }
    }

    public function index()
    {
      $response = $this->client->get($this->api_url,['query' => [
          'jsession'  => $this->jsession,
          'vehiIdno'   => $this->vehiIdno
        ]]);
      $data = $response->getBody();

      echo $data;
    }
}
