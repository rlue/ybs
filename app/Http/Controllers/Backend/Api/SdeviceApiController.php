<?php namespace App\Http\Controllers\Backend\Api;


use Illuminate\Http\Request;
use App\Device;
use DB;
class SdeviceApiController extends ApiController
{ 
    private $user;
    private $password;

    private $db;

    function __construct(Request $request)
    {
      $user = $request->get('user');
      $password = $request->get('password');
      if ($user && $password) {
        $this->user = $user;
        $this->password = $password;
      }else{
        $this->respondWithError('Please check user name and password');
      }
      $this->db = DB::connection('sqlsrv');
    }

    public function index()
    { 
      $query = "select * from dbo.Device
                where ID in(select ud.DeviceID from dbo.UserData as uda
                join dbo.UsrDevice as ud on ud.UserID = uda.ID
                where uda.UserName ='$this->user' and uda.PassWord ='$this->password')";
      $devices = $this->db->select($query);
      return $this->respond([
        'total' => count($devices),
        'data' => $devices
      ]);
    }
}
