<?php namespace App\Http\Controllers\Backend\Api;


use App\Transformers\BusTransformer;
use App\Transformers\BusRouteTransformer;
use Illuminate\Http\Request;
use App\Bus;
use App\BusStop;
use Carbon\Carbon;

class BusApiController extends ApiController
{
    protected $busTransformer;
    protected $busRouteTransformer;

    public function __construct(BusTransformer $busTransformer, BusRouteTransformer $busRouteTransformer)
    {
        $this->busTransformer    = $busTransformer;
        $this->busRouteTransformer    = $busRouteTransformer;
    }


    public function index(Request $request)
    {
        $bus = Bus::with('busstops')->get();
        return $this->respond([
            'data' => $this->busTransformer->transformCollection($bus->all())
      ]);
    }

    public function getBusRoutes(Request $request)
    {
        $name = $request->get('bus_name');
        $bus = Bus::where('name',$name)->first();
        if(! $bus){
           return $this->respondNotFound('Bus not found!');
        }
        $route_start_to_end = $bus->busstops()->wherePivot('type','F')->orderBy('pivot_order', 'asc')->get()->toArray();
        $route_end_to_start = $bus->busstops()->wherePivot('type','R')->orderBy('pivot_order', 'asc')->get()->toArray();
        return $this->respond([
            'data' => [
                'route_start_to_end' => $this->busRouteTransformer->transformCollection($route_start_to_end),
                'route_end_to_start' => $this->busRouteTransformer->transformCollection($route_end_to_start)
            ],
        ]);
    }

    public function getUpdatedTime()
    {
        $bus = Bus::orderBy('updated_at','DESC')->first();
        $bus_stop = BusStop::orderBy('updated_at','DESC')->first();
        return $this->respond([
            'updated_at' => $bus->updated_at->toDateTimeString(),
            'busstop_updated_at' => $bus_stop->updated_at->toDateTimeString(),
        ]);
    }
}
