<?php

namespace App\Http\Controllers\Backend\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\GateRegister;
use App\GateKeeper;
use App\BusGate;
use App\BusState;
class GateRegisterApiController extends ApiController
{
    //

    public function index()
    {

    }

    public function BusRegisterGate(Request $request)
    {

    	$data = [
    		'gateRegister_serviceNo' => $request->get('gateRegister_serviceNo'),
    		"gateRegister_userid" => $request->get('gateRegister_userid'),
    		"gateRegister_gateid" => $request->get('gateRegister_gateid'),
    		"gateRegister_time" =>$request->get('gateRegister_time'),
    		"gateRegister_inout"  =>$request->get('gateRegister_inout'),
    		"gateRegister_busNo"  =>$request->get('gateRegister_busNo'),
            'gateRegister_isArrive' => $request->get('gateRegister_isArrive'),
    		"gateRegister_date"   => $request->get('gateRegister_date')
    	];

    	$gate = GateRegister::create($data);

    	if(! $gate){
           return $this->respondNotFound('Not Success');
        }
    	return $this->respond([
    		'data' => [
                'status' => 'Success',
                
            ],
        ]);
    }
    public function getBusRegisterGate(Request $request)
    {
        $serviceno = $request->get('gateRegister_serviceNo');
        $arrive= $request->get('gateRegister_isArrive');
        $inout = $request->get('gateRegister_inout');


        $getGate = GateRegister::with('bus','gate','user')->where('gateRegister_serviceNo','=',$serviceno)
        ->where('gateRegister_isArrive','=',$arrive)
        ->where('gateRegister_inout','=',$inout)
        ->get()->toArray();
        if(!$getGate){
           return $this->respondNotFound('Not Success');
        }
        return $this->respond([
            'data' => [
                'status' => 'Success',
                'gate_bus' => $getGate
                
            ],
        ]);
    }
    public function busState(Request $request)
    {
    	$data = [
    		'busState_serviceno' => $request->get('busState_serviceno'),
    		"busState_onoff" => $request->get('busState_onoff'),
    		"busState_busno" => $request->get('busState_busno'),
    		"busState_time" =>$request->get('busState_time'),
    		"busState_date"  =>$request->get('busState_date')
    		
    	];

    	$gate = BusState::create($data);

    	
    	return $this->respond([
    		'data' => [
                'status' => 'Success',
                
            ],
        ]);
    }
    public function getBusState(Request $request)
    {
        $date = $request->get('busState_date');
        $onoff = $request->get('busState_onoff');

        $state = BusState::with('bus')
        ->where('busState_date','=',$date)
        ->where('busState_onoff','=',$onoff)
        ->get()->toArray();
        if(!$state){
           return $this->respondNotFound('Not Success');
        }
        return $this->respond([
            'data' => [
                'status' => 'Success',
                'bus_state' => $state
                
            ],
        ]);

    }
    public function getGateUser(Request $request)
    {
        $name = $request->get('gatekeeper_accountName');
        $password = sha1($request->get('gatekeeper_password'));

    	$gate = GateKeeper::with('bus','busgate')->where('gatekeeper_accountName','=',$name)
        ->where('gatekeeper_password','=',$password)
        ->get()->toArray();

    	if(!$gate){
           return $this->respondNotFound('Not Success');
        }
        return $this->respond([
            'data' => [
                'status' => 'Success',
                'gate_keeper' => $gate
                
            ],
        ]);
    }
    public function getGate(Request $request)
    {
    	$gate = BusGate::get()->toArray();

    	return $this->respond([
    		'data' => [
                'status' => 'Success',
                'gate' => $gate
            ],
        ]);
    }
}
