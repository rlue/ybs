<?php namespace App\Http\Controllers\Backend\Api;

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class GuzzleClient
{
    public $client;

    protected $jsession;

    private $base_uri = "http://yangonbus.selfip.net:88/";

    private $login_url = 'StandardApiAction_login.action?';

    private $logout_url = 'StandardApiAction_logout.action?';

    protected $api_url = '';

    public function __construct()
    {
        $this->client = new Client(['base_uri' => $this->base_uri]);
    }



    public function login($userName, $password)
    {
        $data = [
            'account'   => $userName,
            'password'  => $password
        ];
        $response = $this->client->get($this->login_url,['query' => $data]);
        $data = json_decode($response->getBody(),true);
        $this->jsession = isset($data['jsession']) ? $data['jsession'] : '';
        return true;
    }


    public function logout()
    {
        $data = [
            'jsession'   => $this->jsession
        ];
        $response = $this->client->get($this->logout_url,['query' => $data]);
        $data = $response->getBody();
        echo $data;
    }
}
