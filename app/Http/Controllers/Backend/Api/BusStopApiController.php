<?php namespace App\Http\Controllers\Backend\Api;


use App\Transformers\BusStopTransformer;
use Illuminate\Http\Request;
use App\BusStop;

class BusStopApiController extends ApiController
{
    protected $busStopTransformer;

    public function __construct(BusStopTransformer $busStopTransformer)
    {
        $this->busStopTransformer    = $busStopTransformer;
    }


    public function index(Request $request)
    {
        $busstops = BusStop::all();
        return $this->respond([
        'data' => $this->busStopTransformer->transformCollection($busstops->all())
      ]);
    }


}
