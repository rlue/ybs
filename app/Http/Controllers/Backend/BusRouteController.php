<?php

namespace App\Http\Controllers\Backend;

use App\Route;
use App\Bus;
use App\BusStop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Excel;

class BusRouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $routes = Bus::all();
        return view('backend.route.index', compact('routes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bus $bus
     * @return \Illuminate\Http\Response
     */
    public function show(Bus $bus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bus $bus
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bus = Bus::findOrFail($id);

        $route_start_to_end = $bus->busstops()->with(['township','street'])->wherePivot('type','F')->orderBy('pivot_order', 'asc')->get()->toArray();
        $route_end_to_start = $bus->busstops()->with(['township','street'])->wherePivot('type','R')->orderBy('pivot_order', 'asc')->get()->toArray();

        $busstops =json_encode(array_map([$this, 'transformItem'], BusStop::with(['township','street'])->get()->toArray()));
        $route_start_to_end = json_encode(array_map([$this, 'transformItem'], $route_start_to_end));
        $route_end_to_start = json_encode(array_map([$this, 'transformItem'], $route_end_to_start));
        return view('backend.route.update', compact('bus', 'busstops', 'route_start_to_end', 'route_end_to_start'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bus $bus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bus $bus)
    {

        $fbusstops_ids = ($request->get('bus_ids')) ? $request->get('bus_ids') : [];
        $rbusstops_ids = ($request->get('bus_ids1')) ? $request->get('bus_ids1') : [];
        $busstops = [];
        foreach ($fbusstops_ids as $key => $value) {
            $busstops[$value] = ['type' => 'F', 'order' => $key];
        }
        foreach ($rbusstops_ids as $key => $value) {
            $busstops[$value] = ['type' => 'R','order' => $key];
        }
        $bus = Bus::findOrFail($request->get('bus_id'));
        $bus->busstops()->detach();
        $bus->busstops()->attach($busstops);
        if ($bus) {
            flash('Bus Route Updated Successfully!')->success();
            return redirect()->route('route.index');
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bus $bus
     * @return \Illuminate\Http\Response
     */
    public function destroy(Route $route)
    {
        //
    }

    public function transformItem($item)
    {
        $township = isset($item['township']['name']) ? ' (' . $item['township']['name'] . ')' : '';
        $street = isset($item['street']['name']) ? ' ('. $item['street']['name'] . ') ' : '';
        $name = $item['code'] . ' - ' . $item['name'] . $township . $street;
        //$name =  $item['name'];
        return [
            'id'         => $item['id'] ,
            'value'         => $item['id'] ,
            'label'         => $name,
            'name'         => $item['name'],
            'lat'           => $item['lat'] ,
            'long'          => $item['long'] ,
            'has_corner'    => $item['has_corner'] ,
            'code'          => $item['code'],
            'order'         =>  true,
            'listfixed'     =>  false
            ];
    }


    public function getMap(Request $request)
    {
        if ($request->has('formSubmit')) {
            $routes = json_decode($request->get('route_start_to_end'),true);
            $routes1 = json_decode($request->get('route_end_to_start'),true);
        }else{
            if (!$request->has('bus_id')) {
                abort(404);
            }
            $bus = Bus::findOrFail($request->get('bus_id'));
            $routes = $bus->busstops()->wherePivot('type','F')->orderBy('pivot_order', 'asc')->get()->toArray();
            $routes1 = $bus->busstops()->wherePivot('type','R')->orderBy('pivot_order', 'asc')->get()->toArray();
        }
        $data = [
            'froutes' => ($routes) ? json_encode($routes) : json_encode([]),
            'rroutes' => ($routes1) ? json_encode($routes1) : json_encode([]),
        ];
        return view('backend.route.map',compact('data'));
    }


    /**
     * export excel data
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getExportExcel(Request $request)
    {
        $bus = Bus::with(['busstops'])->get();

        $routeArr[0] = ['No', 'Lat,Long' , 'BusStop' , 'Type'];

        $count = 1;
        Excel::create('busstops_export', function ($excel) use ($bus,$routeArr) {
            foreach ($bus as $route) {
                $sheetTitle = str_limit($route->name, 25);
                $excel->sheet($sheetTitle, function ($sheet) use ($route,$routeArr) {
                    $sheet->setFontFamily('Zawgyi-One');
                    $busstops = $route->busstops()->orderBy('pivot_type')->orderBy('pivot_order','asc')->get();
                    foreach ($busstops as $index =>  $stop) {
                        $geo = $stop->lat . ', ' . $stop->long;
                        $routeArr[] = [++$index, $geo, $stop->name , $stop->pivot->type];
                    }
                    $sheet->fromArray($routeArr, null, 'A1', false, false);
                    $sheet->row(1, function ($row) {
                        $row->setFontWeight('bold');
                    });
                });
            }
        })->download('xlsx');
    }
}
