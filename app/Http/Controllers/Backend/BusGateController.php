<?php

namespace App\Http\Controllers\Backend;

use App\BusGate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Controllers\Backend\Traits\Authorizable;
use DB;
class BusGateController extends Controller
{
   //use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gates = BusGate::all();
        return view('backend.gate.index', compact('gates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.gate.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'gate_name'  => 'required',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            flash(implode('</br>', $messages->all()))->error();
            return redirect()->back();
        }
        $gate = BusGate::create($request->all());

        flash('Gate create succefully!')->success();
        return redirect()->route('gate.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $busgate = BusGate::FindOrFail($id);
        return view('backend.gate.update', compact('busgate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
         $name  = $request->input("gate_name");
       
        $gateupdate = ['gate_name' => $name];
       
       
       DB::table('gates')
            ->where('gate_id', $id)
            ->update($gateupdate);
           

        flash('Gate updated succefully!')->success();
        return redirect()->route('gate.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(BusGate $busgate)
    {
        $delete = $busgate->delete();
        if (!$delete) {
            flash('Gate delete fail')->danger();
        }
        flash('Gate deleted successfully!')->success();
        return redirect()->route('gate.index');
    }


    public function getGateList(Request $request)
    {
        $ret = array();
        $ret[] = array("value"=>"", "text"=>"Select Gat");
        $result = BusGate::get();
        if (!empty($result)) {
            foreach ($result as $res) {
                $ret[] = array(
                    "value"    => $res->gate_id,
                    "text"  => $res->gate_name,
                );
            }
        } else {
            $ret[] = array("value"=>"0", "text"=>"No Results Found..");
        }
        echo json_encode($ret);
    }
}
