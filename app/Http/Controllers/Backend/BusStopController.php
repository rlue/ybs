<?php

namespace App\Http\Controllers\Backend;

use App\BusStop;
use App\Street;
use App\Township;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;
use Carbon\Carbon;
use Excel;

class BusStopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $busstops = BusStop::paginate($this->perPage);
        return view('backend.busstop.index',compact('busstops'));
    }

    public function data(Request $request)
    {

        $busstops = BusStop::with('street','township')->get();

        $datatables = Datatables::of($busstops)
                        ->addColumn('no', function ($busstop) {
                            return '';
                        })
                        ->editColumn('name', function($busstop) {
                           return '<a href="#" class="name-edit"  data-type="text" data-pk="'. $busstop->id .'" data-name="name" data-url="/admin/busstop/quickUpdate" data-title="Enter Name">'. $busstop->name .'</a>' ;
                        })
                        ->editColumn('code', function($busstop) {
                           return '<a href="#" class="code-edit"  data-type="text" data-pk="'. $busstop->id .'" data-name="code" data-url="/admin/busstop/quickUpdate" data-title="Enter Code">'. $busstop->code .'</a>' ;
                        })
                        ->editColumn('lat', function($busstop) {
                            return '<a href="#" class="lat-edit"  data-type="text" data-pk="'. $busstop->id .'" data-name="lat" data-url="/admin/busstop/quickUpdate" data-title="Enter Lat">'. $busstop->lat . '</a>' ;
                        })
                      ->editColumn('long', function($busstop) {
                            return '<a href="#" class="long-edit"  data-type="text" data-pk="'. $busstop->id .'" data-name="long" data-url="/admin/busstop/quickUpdate" data-title="Enter Long">'. $busstop->long . '</a>' ;
                        })
                        ->editColumn('township', function($busstop){

                            $name =  isset($busstop->township->name) ? $busstop->township->name : '';
                            $township_id =  isset($busstop->township->id) ? $busstop->township->id : '';
                            return '<a href="#" class="township-edit" data-township-id="'. $township_id  .'"  data-type="select" data-pk="'. $busstop->id .'" data-name="township_id" data-url="/admin/busstop/quickUpdate" data-title="Select Township">'. $name . '</a>' ;

                        })->editColumn('street', function($busstop){

                            $name =  isset($busstop->street->name) ? $busstop->street->name : '';
                            $street_id =  isset($busstop->street->id) ? $busstop->street->id : '';
                            return '<a href="#" class="street-edit" data-street-id="'. $street_id .'"  data-type="select" data-pk="'. $busstop->id .'" data-name="street_id" data-url="/admin/busstop/quickUpdate" data-title="Select Street">'. $name . '</a>' ;

                        })->editColumn('action', function($busstop) {
                           $btn = '';
                            $btn .= ' <a href="' . route('busstop.edit',['busstop' => $busstop]) . '" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> Edit</a>';

                           $btn .= ' <a href="" data-id="'. $busstop->id .'" class="btn btn-danger btn-sm del-link" ><i class="fa fa-trash"></i> Delete</a>';

                           $btn .= " <a target='_blank' class='btn btn-warning btn-sm' href='http://maps.google.com/maps?q=" .$busstop->lat . ','. $busstop->long. "' > <i class='fa fa-map-marker' aria-hidden='true'></i> Map</a>"; 
                             return $btn;
                        });

        if ($keyword = $request->get('search')['value']) {
            $datatables->filterColumn('name', 'whereRaw', "name like ? ", ["%$keyword%"]);
        }
        return $datatables->make(true);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $streets = Street::pluck('name','id')->toArray();
        $townships = Township::pluck('name','id')->toArray();
        return view('backend.busstop.create',compact('streets','townships'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
             'name' => 'required',
             'street_id' => 'required',
             'lat'  => 'required',
             'long' => 'required',
        ]);
        $has_corner = $request->has('has_corner') ? 1 : 0;
        $request->merge(['has_corner' => $has_corner]);

        $bus = BusStop::create($request->all());
        if ($bus) {
            flash('BusStop Created Successfully!')->success();
            return redirect()->route('busstop.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BusStop  $busStop
     * @return \Illuminate\Http\Response
     */
    public function show(BusStop $busStop)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BusStop  $busStop
     * @return \Illuminate\Http\Response
     */
    public function edit(BusStop $busstop)
    {
        $streets = Street::pluck('name','id')->toArray();
        $townships = Township::pluck('name','id')->toArray();
        return view('backend.busstop.update',compact('busstop','streets','townships'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BusStop  $busStop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BusStop $busstop)
    {

        $this->validate($request,[
             'name' => 'required',
             'street_id' => 'required',
             'lat'  => 'required',
             'long' => 'required',
        ]);
        $has_corner = $request->has('has_corner') ? 1 : 0;
        $request->merge(['has_corner' => $has_corner]);
        $request->merge(['updated_at' => Carbon::now()->toDateTimeString()]);
        $bus = $busstop->update($request->all());
        if ($bus) {
            flash('BusStop Updated Successfully!')->success();
            return redirect()->route('busstop.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BusStop  $busStop
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delete = BusStop::findOrFail($id)->delete();
        if ($delete) {
            $ret = [
                'status' => 'success',
                'msg'    => 'Busstop deleted successfully!'
            ];
        }else{
            $ret = [
                'status' => 'fail',
                'msg'    => 'Error in delete busstop'
            ];
        }
        echo json_encode($ret);
    }

    public function quickUpdate(Request $request)
    {
        $input = $request->all();

        $ret = [
                'status' => 'fail',
                'message'    => 'Error in updated busstop'
            ];

        if (isset($input['pk']) && $input['value'] != '') {
            $busstop = BusStop::findOrFail($input['pk']);
            $updated = $busstop->update([ $input['name'] => $input['value']]);

            if($updated) {
                $ret = [
                    'status' => 'success',
                    'message'    => 'Busstop updated successfully!'
                ];
            }
        }

        echo json_encode($ret);

    }


    /**
     * export excel data
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getExportExcel(Request $request)
    {
        $busstops = BusStop::with(['street','township'])->get();

        $busStopArr[0] = ['No', 'Name', 'Code', 'Street', 'Township','Lat','Long', 'Junction'];

        $count = 1;
        if (!empty($busstops)) {
            foreach ($busstops as $stop) {
                $busStopArr[] = [$count++, $stop->name, $stop->code, $stop->getStreetName(), $stop->getTownshipName(),$stop->lat, $stop->long, $stop->hasJunction()];
            }
        }
        Excel::create('busstops_export', function ($excel) use ($busStopArr) {
            $excel->sheet('Sheetname', function ($sheet) use ($busStopArr) {
                $sheet->setFontFamily('Zawgyi-One');
                $sheet->fromArray($busStopArr, null, 'A1', false, false);
                $sheet->row(1, function ($row) {
                    $row->setFontWeight('bold');
                });
            });
        })->download('xlsx');
    }
}
