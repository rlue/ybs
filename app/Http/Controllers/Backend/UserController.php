<?php

namespace App\Http\Controllers\Backend;

use App\User;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Controllers\Backend\Traits\Authorizable;
class UserController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::paginate(config('backend.perPage'));
        return view('backend.user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name', 'name')->toArray();
        return view('backend.user.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|unique:users',
            'email' => 'required|unique:users',
            'password' => 'required',
            'roles'      => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            flash(implode('</br>', $messages->all()))->error();
            return redirect()->back();
        }
        $user = User::create([
                                'name' => $request->get('name'),
                                'email' => $request->get('email'),
                                'password' => bcrypt($request->get('password'))
                             ]);
        $user->syncRoles($request->get('roles'));
        //$user->syncPermissions($request->get('permissions'));
        flash('User create succefully!')->success();
        return redirect()->route('user.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::pluck('name','name')->toArray();
        $userRoles = $user->roles()->pluck('name')->toArray();
        return view('backend.user.update', compact('user','roles','userRoles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|unique:users,name,' . $user->id ,
            'email' => 'required|unique:users,email,' . $user->id,
            'roles'      => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            flash(implode('</br>', $messages->all()))->error();
            return redirect()->back();
        }
        $password = $user->password;

        if($request->has('password')){
            $password = bcrypt($request->get('password'));
        }
        $user->update([
                        'name' => $request->get('name'),
                        'email' => $request->get('email'),
                        'password' => $password
                     ]);
        $user->syncRoles($request->get('roles'));
        //$user->syncPermissions($request->get('permissions'));
        flash('User updated succefully!')->success();
        return redirect()->route('user.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $delete = $user->delete();
        if (!$delete) {
            flash('User delete fail')->danger();
        }
        flash('User deleted successfully!')->success();
        return redirect()->route('user.index');
    }

    public function profile()
    {
        $id = auth()->id();
        if (!$id) {
           abort('403') ;
        }
        $user = User::findOrFail($id);
        return view('backend.user.profile',compact('user'));
    }

    public function updateProfile(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|unique:users,name,' . $user->id ,
            'email' => 'required|unique:users,email,' . $user->id
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            flash(implode('</br>', $messages->all()))->error();
            return redirect()->back();
        }
        $password = $user->password;

        if($request->has('password')){
            $password = bcrypt($request->get('password'));
        }
        $user->update([
                        'name' => $request->get('name'),
                        'email' => $request->get('email'),
                        'password' => $password
                     ]);
        flash('Profile Updated Succefully!')->success();
        return redirect()->route('user.profile');
    }
}
