<?php

namespace App\Http\Controllers\Backend;

use App\Bus;
use App\BusStop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class BusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $buses = Bus::all();
        return view('backend.bus.index',compact('buses'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $busstops = BusStop::pluck('name','id')->toArray();
        return view('backend.bus.create',compact('busstops'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'  => 'required|unique:bus',
            'user'  => 'required',
            'password' => 'required',
            'server' => 'required'
        ]);

        $bus = Bus::create($request->all());
        $bus->busstops()->sync($request->get('bus_stops'));
        if ($bus) {
            flash('Bus Created Successfully!')->success();
            return redirect()->route('bus.index');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Bus $bus
     * @return \Illuminate\Http\Response
     */
    public function show(Bus $bus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Bus $bus
     * @return \Illuminate\Http\Response
     */
    public function edit(Bus $bus)
    {
        $busstops = BusStop::pluck('name','id')->toArray();
        return view('backend.bus.update',compact('bus','busstops'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Bus $bus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Bus $bus)
    {
        $this->validate($request,[
            'name'  => 'required|unique:bus,name,' . $bus->id,
            'user'  => 'required',
            'password' => 'required',
            'server' => 'required',
        ]);
        $bus->busstops()->sync($request->get('bus_stops'));
        $request->merge(['updated_at' => Carbon::now()->toDateTimeString()]);
        $bus = $bus->update($request->all());
        if ($bus) {
            flash('Bus Updated Successfully!')->success();
            return redirect()->route('bus.index');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Bus $bus
     * @return \Illuminate\Http\Response
     */
    public function destroy(Bus $bus)
    {
        $delete = $bus->delete();
        if ($delete) {
            flash('Bus Deleted Successfully!')->success();
            return redirect()->route('bus.index');
        }
    }
}
