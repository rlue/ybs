<?php

namespace App\Http\Controllers\Backend;

use App\Township;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Controllers\Backend\Traits\Authorizable;
class TownshipController extends Controller
{
   //use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $townships = Township::all();
        return view('backend.township.index', compact('townships'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.township.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|unique:townships',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            flash(implode('</br>', $messages->all()))->error();
            return redirect()->back();
        }
        $township = Township::create($request->all());

        flash('Township create succefully!')->success();
        return redirect()->route('township.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(Township $township)
    {
           
        return view('backend.township.update', compact('township'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Township $township)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|unique:townships,name,' . $township->id ,
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            flash(implode('</br>', $messages->all()))->error();
            return redirect()->back();
        }

        $township->update($request->all());

        flash('Township updated succefully!')->success();
        return redirect()->route('township.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(Township $township)
    {
        $delete = $township->delete();
        if (!$delete) {
            flash('Township delete fail')->danger();
        }
        flash('Township deleted successfully!')->success();
        return redirect()->route('township.index');
    }


    public function getTownshipList(Request $request)
    {
        $ret = array();
        $ret[] = array("value"=>"", "text"=>"Select Township");
        $result = Township::get();
        if (!empty($result)) {
            foreach ($result as $res) {
                $ret[] = array(
                    "value"    => $res->id,
                    "text"  => $res->name,
                );
            }
        } else {
            $ret[] = array("value"=>"0", "text"=>"No Results Found..");
        }
        echo json_encode($ret);
    }
}
