<?php

namespace App\Http\Controllers\Backend;

use App\Street;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Controllers\Backend\Traits\Authorizable;

class StreetController extends Controller
{
   //use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $streets = Street::all();
        return view('backend.street.index', compact('streets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.street.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|unique:streets',
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            flash(implode('</br>', $messages->all()))->error();
            return redirect()->back();
        }
        $street = Street::create($request->all());

        flash('street create succefully!')->success();
        return redirect()->route('street.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(street $street)
    {
        return view('backend.street.update', compact('street'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, street $street)
    {
        $validator = Validator::make($request->all(), [
            'name'  => 'required|unique:streets,name,' . $street->id ,
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            flash(implode('</br>', $messages->all()))->error();
            return redirect()->back();
        }

        $street->update($request->all());

        flash('street updated succefully!')->success();
        return redirect()->route('street.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(street $street)
    {
        $delete = $street->delete();
        if (!$delete) {
            flash('street delete fail')->danger();
        }
        flash('street deleted successfully!')->success();
        return redirect()->route('street.index');
    }


    public function getStreetList(Request $request)
    {
        $ret = array();
        $ret[] = array("value"=>"", "text"=>"Select Street");
        $result = Street::get();
        if (!empty($result)) {
            foreach ($result as $res) {
                $ret[] = array(
                    "value"    => $res->id,
                    "text"  => $res->name,
                );
            }
        } else {
            $ret[] = array("value"=>"0", "text"=>"No Results Found..");
        }
        echo json_encode($ret);    
    }
}
