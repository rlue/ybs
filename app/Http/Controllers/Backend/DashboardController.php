<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Bus;
use App\BusStop;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Backend\Traits\Authorizable;
class DashboardController extends Controller
{
    //use Authorizable;

    public function index()
    {   
        $buses = Bus::all();
        $busstops = BusStop::all();
        $data = [
          'totalBus'  => count($buses),
          'totalBusStop' => count($busstops)
        ];
        return view('backend.dashboard',compact('data'));
    }
}
