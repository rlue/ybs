<?php

namespace App\Http\Controllers\Backend;

use App\GateKeeper;
use App\Bus;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Controllers\Backend\Traits\Authorizable;
use DB;
use App\BusGate;
class GateKeeperController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $keeper = GateKeeper::with(['bus','busgate'])->get();
         
        return view("backend.gatekeeper.index", compact('keeper'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $bus = Bus::get();
        $gate = BusGate::get();
        return view("backend.gatekeeper.create",compact('bus','gate'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $validator = Validator::make($request->all(), [
            'gatekeeper_name'  => 'required|unique:gate_keeper',
            'service_number' => 'required',
            'gatekeeper_password' => 'required',
            'gatekeeper_accountName'      => 'required',
            'gatekeeper_gateid' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->errors();
            flash(implode('</br>', $messages->all()))->error();
            return redirect()->back();
        }

        $data = [
                    'gatekeeper_name' => $request->get('gatekeeper_name'),
                    'gatekeeper_gateid' => $request->get('gatekeeper_gateid'),
                    'service_number' => $request->get('service_number'),
                    'gatekeeper_password' => sha1($request->get('gatekeeper_password')),
                    'gatekeeper_accountName' => $request->get('gatekeeper_accountName')

                ];
        $productid = GateKeeper::create($data);
                        
        
        flash('Gate Keeper create succefully!')->success();
        return redirect()->route('gatekeeper.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
         $gatekeeper = DB::table('gate_keeper')->where('gatekeeper_id','=',$id)->first();

          $bus = Bus::get();
          $gate = BusGate::get();
        return view('backend.gatekeeper.update',compact('gatekeeper','bus','gate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $input = $request->except('roles');
        
        $gatekeeper_name  = $request->input("gatekeeper_name");
        $service_number     = $request->input('service_number');
        $gatekeeper_accountName     = $request->input('gatekeeper_accountName');
        $gatekeeper_gateid     = $request->input('gatekeeper_gateid');
        $password  = $request->input('gatekeeper_password');
        
        if(isset($password) && !empty($password)){
            $gatekeeper = ['gatekeeper_name' => $gatekeeper_name,'gatekeeper_accountName' => $gatekeeper_accountName,'service_number' => $service_number,'gatekeeper_gateid' => $gatekeeper_gateid,'gatekeeper_password'=>sha1($password)];

        }else{
             $gatekeeper = ['gatekeeper_name' => $gatekeeper_name,'gatekeeper_accountName' => $gatekeeper_accountName,'gatekeeper_gateid' => $gatekeeper_gateid,'service_number' => $service_number];
        }
       
       DB::table('gate_keeper')
            ->where('gatekeeper_id', $id)
            ->update($gatekeeper);
            flash('Gate Keeper Update succefully!')->success();
        return redirect()->route('gatekeeper.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        DB::table('gate_keeper')->where('gatekeeper_id','=',$id)->delete();
        flash('Gate Keeper Delete succefully!')->success();
        return redirect()->route('gatekeeper.index');
    }
}
