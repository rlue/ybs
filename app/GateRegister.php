<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GateRegister extends Model
{
    //
    protected $table = 'gate_register';

    protected $primaryKey = 'gateRegister_id';

    protected $fillable = ['gateRegister_serviceNo','gateRegister_userid','gateRegister_gateid','gateRegister_time','gateRegister_inout','gateRegister_busNo','gateRegister_date','gateRegister_isArrive'];

    public function bus()
    {
        return $this->belongsTo(Bus::class,'gateRegister_serviceNo');
    }
    public function user()
    {
        return $this->belongsTo(User::class,'gateRegister_userid');
    }
    public function gate()
    {
        return $this->belongsTo(BusGate::class,'gateRegister_gateid');
    }
}
