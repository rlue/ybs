<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Bus;
use App\Street;
use App\Township;

class BusStop extends Model
{
    protected $table = 'bus_stops';

    protected $fillable = ['name','code','label','street_id','lat','long','township_id','has_corner','updated_at'];

    public function bus()
    {
        return $this->belongsToMany(Bus::class, 'bus_busstop', 'busstop_id', 'bus_id');
    }

    public function street()
    {
        return $this->belongsTo(Street::class);
    }

    public function township()
    {
        return $this->belongsTo(Township::class);
    }


    public function hasJunction()
    {
        return ($this->has_corner) ? 'Yes' : '';
    }



    public function getStreetName()
    {
        $street = '';
        if (isset($this->street->name) ){
            $street = $this->street->name;
        }
        return $street;
    }


    public function getTownshipName()
    {
        $township = '';
        if (isset($this->township->name)) {
            $township = $this->township->name;
        }
        return $township;
    }
}
