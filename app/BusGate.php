<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusGate extends Model
{
    //
    protected $table = 'gates';
    protected $primaryKey = 'gate_id';
    protected $fillable = ['gate_name'];
}
