<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GateKeeper extends Model
{
    //

	protected $table = 'gate_keeper';

    protected $primaryKey = 'gatekeeper_id';
    protected $fillable = ['gatekeeper_name','service_number','gatekeeper_password','gatekeeper_accountName','gatekeeper_gateid'];

    public function bus()
    {
        return $this->belongsTo(Bus::class,'service_number');
    }

    public function busgate()
    {
        return $this->belongsTo(BusGate::class,'gatekeeper_gateid');
    }
}
