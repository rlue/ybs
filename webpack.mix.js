const {mix} = require('laravel-mix');

//.js('resources/assets/js/app.js', 'public/js')

// Admin
mix
    .sass('resources/backend/scss/style.scss', 'public/assets/backend/css')
    .sass('resources/frontend/sass/style.scss', 'public/assets/frontend/css')
    .options({
        processCssUrls: false
    });
