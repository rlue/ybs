<?php


return [

    'backend_url' => 'admin',

    'perPage'     => 10,

    'permissions' => [
        'dashbaord'      => ['browse_dashboard','browse_admin'],
        'user'           => ['browse_user','create_user','update_user','delete_user'],
        'role'           => ['browse_role','create_role','update_role','delete_role'],
        'permission'     => ['browse_permission','create_permission','update_permission','delete_permission'],
        'product'        => ['browse_product','create_product','update_product','delete_product']
    ],

    'allow_hosts' => [
        'http://118.200.16.205:5588',
        'http://118.200.16.205',
        '118.200.16.205',
        'ybs.dev',
        'yangonbus.selfip.net'
    ],

];
