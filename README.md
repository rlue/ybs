# Laravel PHP Framework

![Bulb](https://image.ibb.co/f8Pwkv/bulb.png)

## Setup

```bash
# Backend
chmod 777 -R storage bootstrap/cache
composer update
cp .env.example .env && php artisan key:generate

# Development
yarn
bower install(in corresponding assets directory - public/assets/backend/)
npm run dev
```

## Sublime Text Plugins

1. EditorConfig
2. Babel
3. Laravel Blade Highlighter

## Frontend Dev Commands

```
npm run dev
npm run watch
npm run prod
```
